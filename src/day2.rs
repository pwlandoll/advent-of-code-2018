//use std::collections::HashSet;
use std::char;
use std::io::Result;

pub fn part1(input: &String) -> Result<()> {
    let mut count_twos = 0;
    let mut count_threes = 0;
    let mut count_two_in_line = false;
    let mut count_three_in_line = false;
    let mut count: usize;
    let alphabet = "abcdefghijklmnopqrstuvwxyz";
    for line in input.lines() {
        for letter in alphabet.chars() {
            count = line.matches(letter).count();
            if count == 2 {
                count_two_in_line = true;
            } else if count == 3 {
                count_three_in_line = true;
            }
        }
        if count_two_in_line {
            count_twos += 1;
        }
        if count_three_in_line {
            count_threes += 1;
        }
        count_two_in_line = false;
        count_three_in_line = false;
    }
    let checksum = count_twos * count_threes;
    println!("{} * {} = {}", count_twos, count_threes, checksum);
    Ok(())
}

pub fn part2(input: &String) -> Result<()> {
    Ok(())
}
