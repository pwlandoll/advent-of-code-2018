use std::collections::HashSet;
use std::io::Result;

pub fn part1(input: &String) -> Result<()> {
    let mut frequency = 0;
    let mut input_int: i32;
    for line in input.lines() {
        input_int = line.parse::<i32>().unwrap();
        frequency += input_int;
    }
    println!("{}", frequency);
    Ok(())
}

pub fn part2(input: &String) -> Result<()> {
    let mut frequency = 0;
    let mut frequencies = HashSet::new();
    frequencies.insert(0);
    let mut input_int: i32;
    //let inputs = read_input()?;
    //let lines = BufReader::new(inputs).lines();
    loop {
        for line in input.lines() {
            input_int = line.parse::<i32>().unwrap();
            frequency += input_int;
            if frequencies.contains(&frequency) {
                println!("{}", frequency);
                return Ok(());
            } else {
                frequencies.insert(frequency);
            }
        }
    }
}
