mod day1;
mod day2;

use std::fs;

fn d1() {
    println!("Day 1:");
    let day1_input = fs::read_to_string("inputs/day1.txt").expect("Unable to read file.");
    day1::part1(&day1_input).expect("Day 1 part 1.");
    day1::part2(&day1_input).expect("Day 1 part 2.");
}

fn d2() {
    println!("Day 2:");
    let day2_input = fs::read_to_string("inputs/day2.txt").expect("Unable to read file.");
    day2::part1(&day2_input).expect("Day 2 part 1.");
    day2::part2(&day2_input).expect("Day 2 part 2.");
}

fn main() {
    //d1();
    d2();
}
